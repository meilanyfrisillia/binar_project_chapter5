import React, { useState } from "react";
import { Form, Button, Container, Row, Col, Card } from "react-bootstrap";
import axios from "axios";
import NavbarComponent from "../components/Header/NavbarComponent";
import { toast } from "react-toastify";
import Google from "../components/Auth/Google";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = async (e) => {
    e.preventDefault();

    try {
      let data = JSON.stringify({
        email,
        password,
      });

      let config = {
        method: "post",
        url: "https://km4-challenge-5-api.up.railway.app/api/v1/auth/login",
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };

      const response = await axios.request(config);
      const { token } = response.data.data;

      localStorage.setItem("token", token);

      // navigate("/");

      //Temporary Solution
      window.location.href = "/";
    } catch (error) {
      if (axios.isAxiosError(error)) {
        toast.error(error.response.data.message);
        return;
      }
      toast.error(error.message);
    }
  };

  return (
    <>
      <div className="vh-100">
        <NavbarComponent />

        <Container className="p-5 mt-5">
          <Card className="rounded-0">
            <Card.Body>
              <h3
                className="text-start text-black"
                style={{
                  borderBottom: "1px solid #eee",
                  paddingBottom: "15px",
                }}
              >
                Login In to Your Account
              </h3>
              <Form onSubmit={onSubmit}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label className="text-light ">Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label className="text-light">Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                <Row>
                  <Col className="text-center">
                    <Button
                      variant="danger"
                      type="submit"
                      className="rounded-5 w-25"
                    >
                      Submit
                    </Button>
                  </Col>
                </Row>
              </Form>
              <h5 className="text-center">or</h5>
              <Row>
                <Col className="text-center">
                  <Google buttonText="Login with Google 🚀" />
                </Col>
              </Row>
            </Card.Body>
          </Card>
        </Container>
      </div>
    </>
  );
}

export default Login;
